FROM node:19.8

WORKDIR /var/secrets
COPY . .

RUN npm install

EXPOSE 8080
CMD [ "npm", "start" ]