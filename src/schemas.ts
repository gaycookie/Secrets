import { ObjectId } from "mongoose";

export interface MessageSchema {
  _id?: ObjectId;
  _hash: string;
  _message: string;
}