export class Constants {
  static readonly SERVER_PORT = 3000;
  static readonly MONGO_URI = "mongodb://localhost:27017/secrets";
  static readonly MAX_BODY_SIZE = "1mb";

  static readonly BEGIN_PGP_MESSAGE_REG = new RegExp('^-----BEGIN PGP MESSAGE-----');
  static readonly END_PGP_MESSAGE_REG = new RegExp('-----END PGP MESSAGE-----\r?\n?$');
}