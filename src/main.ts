import express from "express";
import mongoose, { Connection } from "mongoose";
import { Constants } from "./constants";

import indexRoute from "./routes/index";
import apiRoute from "./routes/api";

export class Main {
  private mongoUri = process.env.MONGO_URI || Constants.MONGO_URI;
  readonly server = express();
  readonly database: Connection;

  constructor() {
    this.initializeMiddleware();
    this.initializeRoutes();

    mongoose.connect(this.mongoUri);
    this.database = mongoose.connection;
    this.database.on('error', () => console.log('Error connecting to the database!'));
    this.database.once('open', () => console.log('Successfully connected to the database!'));
    this.server.listen(Constants.SERVER_PORT, () => console.log(`Webserver listening on port ${Constants.SERVER_PORT}!`));
  }

  private initializeMiddleware() {
    this.server.use(express.json());
    this.server.use(express.text());
    this.server.use(express.urlencoded({ extended: true, limit: Constants.MAX_BODY_SIZE }));
  }

  private initializeRoutes() {
    this.server.use("/api", apiRoute(this));
    this.server.use("/", indexRoute(this));
  }
}

new Main();