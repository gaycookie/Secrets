import crypto from "crypto";
import { Router } from "express";
import { Main } from "../main";
import { MessageSchema } from "../schemas";
import { Constants } from "../constants";

export default (main: Main) => {
  const router = Router();

  router.post('/message', async (req, res) => {
    if (!req.headers["content-type"]) return res.status(400).json({ error: "Missing 'Content-Type' header." });
    if (req.headers["content-type"] !== 'text/plain') return res.status(400).json({ error: "Invalid 'Content-Type' header." });
    if (!req.body) return res.status(400).json({ error: "Missing request body." });
    if (!Constants.BEGIN_PGP_MESSAGE_REG.test(req.body) || !Constants.END_PGP_MESSAGE_REG.test(req.body)) return res.status(400).json({ error: "Invalid PGP message." });

    const insert = await main.database.collection<MessageSchema>('messages').insertOne({ _hash: crypto.randomBytes(8).toString('hex'), _message: req.body });
    if (!insert) return res.status(500).json({ error: 'Something went wrong, message was not inserted.' });

    const result = await main.database.collection<MessageSchema>('messages').findOne({ _id: insert.insertedId });
    if (!result) return res.status(500).json({ error: 'Something went wrong, message was not inserted.' });
    return res.status(200).json({ error: null, hash: result._hash });
  });

  router.get('/message/:hash/:method?', async (req, res) => {
    res.setHeader('Content-Type', 'text/plain');

    const result = await main.database.collection<MessageSchema>('messages').findOne({ _hash: req.params.hash });
    if (!result) return res.status(404).send('Oops, message was not found.');

    if (req.params.method === 'download') {
      res.setHeader('Content-Disposition', `attachment; filename=${result._hash}.txt.gpg`);
    };

    return res.send(result._message);
  });

  return router;
}